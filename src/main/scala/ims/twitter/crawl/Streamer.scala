package ims.twitter.crawl

import java.io.{File, PrintWriter}
import java.text.SimpleDateFormat
import java.util.Calendar

import twitter4j._
import twitter4j.auth.AccessToken

import scala.io.Source

import collection.JavaConverters._

/**
 * Created by rklinger on 4/1/16.
 */
class MyStatusListener(writer:PrintWriter) extends StatusListener {
  def onStallWarning(warning: StallWarning) {
    System.err.println("Got stall warning:" + warning);
  }
  def onException(ex: Exception) {
    ex.printStackTrace();
  }
  def onStatus(status:Status) {
    Streamer.printStatus(status,writer)
  }
  def onDeletionNotice(statusDeletionNotice:StatusDeletionNotice) {
    //System.err.println("Got a status deletion notice id:" + statusDeletionNotice.getStatusId());
  }
  def onTrackLimitationNotice(numberOfLimitedStatuses:Int) {
    System.err.println("Got track limitation notice:" + numberOfLimitedStatuses);
  }
  def onScrubGeo(userId:Long, upToStatusId:Long) {
    System.err.println("Got scrub_geo event userId:" + userId + " upToStatusId:" + upToStatusId);
  }
}

object Streamer {
  val dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z")
  
  val filterQuery = new FilterQuery()
  //makeFilterQuery.track()

  def printStatus(status:Status,writer:PrintWriter) {
    val time = dt.format(status.getCreatedAt)
    val userStrings = ("@"+status.getUser.getScreenName+"\t"+status.getUser.getLang+"\t"+status.getUser.getName.replaceAll("\\t"," ")).replaceAll("[\\n\\r]"," ")
    val tweetID = status.getId
    val tweetText = status.getText.replaceAll("\t","[TAB]").replaceAll("[\\n\\r]","[NEWLINE]")
    val geolocation = if (status.getGeoLocation != null) status.getGeoLocation.getLatitude+";"+status.getGeoLocation.getLongitude else "-1;-1"
    val place = if (status.getPlace != null) status.getPlace.getFullName.replaceAll("[\\n\\t\\r]","_")+"; "+status.getPlace.getCountry else "n/a"

    writer.println("stream\t"+"\t"+time+"\t"+tweetID+"\t"+"\t"+userStrings+"\t"+tweetText+"\t"+geolocation+"\t"+place)
  }
  
  def prepareFilterQuery(keywords:Array[String]) : FilterQuery = {
    val filterQuery = new FilterQuery
    //filterQuery.track(java.util.Arrays.asList(keywords).toArray)
    //filterQuery.track(keywords)
    filterQuery.track(keywords:_*)
    //filterQuery.track(Array("#happy"))
    filterQuery.language(Array("de","en","en-gb","tr"):_*)
//    filterQuery.filterLevel("none")
    filterQuery
  }
  
  def setUpStream(writer:PrintWriter) : TwitterStream = {
    val config = Source.fromFile("tokens.config").getLines().toArray
    val twitterStream: TwitterStream  = new TwitterStreamFactory().getInstance();
    val listener = new MyStatusListener(writer)
    val accessToken = new AccessToken(config(0), config(1))
    twitterStream.setOAuthConsumer(config(2), config(3));
    twitterStream.setOAuthAccessToken(accessToken);
    twitterStream.addListener(listener)
    twitterStream
  }
  
  def closeOutFile(writer:PrintWriter,outFile:File,finalFile:File) = {
    System.err.println("Closing files.")
    writer.flush()
    writer.close()
    System.err.println("Renaming "+outFile.getAbsolutePath+" to "+finalFile.getAbsolutePath)
    outFile.renameTo(finalFile)
  }
  
  def setupOutFile(outfolder:String) : (PrintWriter,File,File) = {
    val f = new File(outfolder) ;
    if (!(f.exists && f.isDirectory)) {
      System.err.println("Make directories: "+f.getAbsolutePath)
      f.mkdirs()
    }
    val now = Calendar.getInstance().getTime
    val calFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss")
    val outFile = new File(outfolder+File.separator+calFormat.format(now)+"-stream.csv.inwork")
    val finalFile = new File(outfolder+File.separator+calFormat.format(now)+"-stream.csv")
    
    val writer = new PrintWriter(outFile)

    (writer,outFile,finalFile)
  }

  def now : String = dt.format(Calendar.getInstance().getTime)
  
  def main(args:Array[String]) : Unit = {
    System.err.println("Starting Twitter Streamer")
    if (args.length < 3){
      System.err.println("Parameters: termfile outfolder minutesToStream")
      System.exit(1)
    }
    val queryFile = args(0)
    System.err.println("Crawling live!")
    System.err.println("Reading query file from "+queryFile)

    // read keywords
    val keywords = Source.fromFile(queryFile).getLines()

    // set up output
    val (writer,outfilename,finalfile) = setupOutFile(args(1))
    
    // how long to stream
    val minutes = args(2).toLong
    
    try {
      val filterQuery = prepareFilterQuery(keywords.toArray)
      val streamer = setUpStream(writer)
      System.err.println("Start streaming for "+minutes+" "+{ if (minutes == 1) "minute" else "minutes"}+" (started "+now+")")
      streamer.filter(filterQuery)
      Thread.sleep(minutes*1000*60)
      System.err.println("Shutting down.")
      streamer.shutdown()
      closeOutFile(writer,outfilename,finalfile)
    }
    catch {
      case e: Exception => {
        System.err.println("General problem!");
        e.printStackTrace();
        Thread.sleep(10000)
      }
    }
  }
}
