package ims.twitter.crawl

import java.text.SimpleDateFormat
import java.util.{Calendar, Date}

import twitter4j.auth.AccessToken

import scala.collection.JavaConversions._
import twitter4j._

import scala.io.Source

/**
 * Created by rklinger on 4/1/16.
 */
object Crawler {
  val config = Source.fromFile("tokens.config").getLines().toArray
  val sdf = new SimpleDateFormat("HH:mm:ss");
  val dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
  val twitter:Twitter = TwitterFactory.getSingleton();
  val accessToken = new AccessToken(config(0), config(1))
  twitter.setOAuthConsumer(config(2), config(3));
  twitter.setOAuthAccessToken(accessToken);

  def search(label:String,term:String,numPages:Int,fromdate:String,untildate:String) = {
    var query:Query = new Query(term)
    query.setCount(100)
    query.setSince(fromdate)
    query.setUntil(untildate)

    System.err.println("First search for "+term)
    var result:QueryResult = tryQueryPrintWait(query,label,term,null)

    for (i <- 1 to numPages ; if (result != null && result.hasNext())) //there is more pages to load
    {
      query = result.nextQuery();
      result = tryQueryPrintWait(query,label,term,result)
    }
  }

  def tryQueryPrintWait(query:Query,label:String,term:String, previousResult:QueryResult) : QueryResult = {
    try {
      val state = checkRateLimitAndWait
      val result = twitter.search(query);
      printResults(result,label,term)
      System.err.println("Number of results for "+term+" ("+state+"): "+result.getTweets.size())
      Thread.sleep(2000)
      result
    } catch {
      case e: TwitterException => {System.err.println("Twitter Exception: "+e.getErrorMessage); e.printStackTrace(System.err); Thread.sleep(10000);previousResult}
      case e: Exception => {System.err.println("Weird Exception: "+e.getMessage) ; e.printStackTrace(System.err);Thread.sleep(10000);previousResult}
    }
  }

  def checkRateLimitAndWait : String = {
    System.err.print("*")
    val feedback = twitter.getRateLimitStatus.get("/search/tweets")
    val remaining = feedback.getRemaining
    val all = feedback.getLimit
    val remainingSec = feedback.getSecondsUntilReset
    //System.err.println("Check for remainder, result = "+remaining)
    if (remaining < 50) {
      System.err.println("Remaining queries: "+remaining+"/"+all+" ; waiting for seconds "+remainingSec+" ("+Math.round(remainingSec/60)+" min at "+sdf.format(Calendar.getInstance.getTime())+")")
      Thread.sleep((remainingSec+60)*1000)
      System.err.println("Continue.")
    }
    "["+remaining+"/"+all+" ; "+remainingSec+"]"
  }

  def printResults(result:QueryResult,label:String,term:String) = {
    for (status:Status <- result.getTweets().toList) {
      val time = dt.format(status.getCreatedAt)
      val userStrings = ("@"+status.getUser.getScreenName+"\t"+status.getUser.getLang+"\t"+status.getUser.getName.replaceAll("\\t"," ")).replaceAll("[\\n\\r]"," ")
      val tweetID = status.getId
      val geolocation = if (status.getGeoLocation != null) status.getGeoLocation.getLatitude+";"+status.getGeoLocation.getLongitude else "-1;-1"
      val place = if (status.getPlace != null) status.getPlace.getFullName.replaceAll("[\\n\\t\\r]","_")+"; "+status.getPlace.getCountry else "n/a"
      val tweetText = status.getText.replaceAll("\t","[TAB]").replaceAll("[\\n\\r]","[NEWLINE]")
      System.out.println(label+"\t"+term+"\t"+time+"\t"+tweetID+"\t"+"\t"+userStrings+"\t"+tweetText+"\t"+geolocation+"\t"+place)
    }
  }

  def main (args: Array[String]) {
    System.err.println("Starting Twitter Emotion Crawler")
//    println(twitter.getRateLimitStatus())
//    System.exit(0)
    if (args.length < 3){
      System.err.println("Parameters: file (each line with label[TAB]query, from and to date as YYYY-MM-DD.")
      System.exit(1)
    }
    val queryFile = args(0)
    val fromdate = args(1)
    val untildate = args(2)
    val maxNumPages = if (args.length > 3) args(3).toInt else 1500
    System.err.println("Crawling from "+fromdate+" until "+untildate)
    System.err.println("Reading query file from "+queryFile)
    System.err.println("Querying for each term maximal pages:"+maxNumPages)

    for(line <- Source.fromFile(queryFile).getLines()) {
      val linesplit = line.split("\\t")
      if (linesplit.length > 1) {
        val label = linesplit(0)
        val query = linesplit(1)
        System.err.println("Query for " + label + " with " + query)
        try {
          search(label, query, maxNumPages, fromdate, untildate)
        }
        catch {
          case e: Exception => {
            System.err.println("General problem!");
            e.printStackTrace();
            Thread.sleep(10000)
          }
        }
      }
    }
  }
}
