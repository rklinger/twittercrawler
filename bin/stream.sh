#!/bin/bash
BASE=`dirname $0`/..

FILENAME=$1
OUTFOLDER=$2
MINUTES=$3

java -Xmx2g -cp $BASE/target/crawl-0.1-jar-with-dependencies.jar ims.twitter.crawl.Streamer $FILENAME $OUTFOLDER $MINUTES
