#!/bin/bash
BASE=`dirname $0`/..
java -Xmx2g -jar $BASE/target/crawl-0.1-jar-with-dependencies.jar $@

# example:
#  bin/crawl.sh emotion-query.txt 2016-04-01 2016-04-02 > data/2016-04-01_02a.csv
