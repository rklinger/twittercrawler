#!/bin/bash
BASE=`dirname $0`/..

FILENAME=$1
PREVIOUS=$2
DAYAFTERPREVIOUS=$(($2+1))
PAGES=$3
OUTFILE=$BASE/data/`gdate -d "$PREVIOUS day 13:00 " '+%Y-%m-%d'`_`gdate -d "-$DAYAFTERPREVIOUS day 13:00" '+%d'`-at-`gdate '+%Y-%m-%d-%H-%M'`-`basename $1 .txt`.csv

java -Xmx2g -jar $BASE/target/crawl-0.1-jar-with-dependencies.jar $1 `gdate -d "$PREVIOUS day 13:00 " '+%Y-%m-%d'` `gdate -d "$DAYAFTERPREVIOUS day 13:00" '+%Y-%m-%d'` $PAGES  > $OUTFILE.inwork
mv $OUTFILE.inwork $OUTFILE
